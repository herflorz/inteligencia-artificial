/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia2_algoritmoediferencial;

/**
 *
 * @author Herminio
 */
public class IA2_AlgoritmoEDiferencial {
    public static int tamanioPoblacion = 20; //Debe ser un número par
    public static int numVariables = 2; // Se indica el número de variables
    public static int numGeneraciones = 50;  //Número de iteraciones
    public static double F = 0.5; //Mutación
    public static double CR = 0.5; //Cruza 

    public static Variable varX = new Variable(0.1, 5.0); //Variable X
    public static Variable varY = new Variable(0.0, 2.0); //Variable Y
    
    public static double poblacionPadres[][] = new double[tamanioPoblacion][numVariables];
    public static double vectoresSeleccionados[][] = new double[3][numVariables];
    public static double vectorRuido[][] = new double[1][numVariables];
    public static double vectorTrial[][] = new double[1][numVariables];
    public static double poblacionHijos[][] = new double[tamanioPoblacion][numVariables];
    
    public static double obtenerFuncion(double x, double y){
        //return (6.983 * Math.pow(x, 2)) + (12.415 * Math.pow(y, 2)) - x;
        //return Math.pow(x, 2) + (2 * Math.pow(y, 2)) - (4 * x) - (2 * x * y);
        return (25 * Math.pow(x, 2)) + (20 * Math.pow(y, 2)) - (2 * x) - y;
    }
    
    public static double repararVariable(Variable var, double valor){
        boolean encontrado = false;
        double valor_reparado = 0;
        if((valor >= var.getLim_inf()) && (valor <= var.getLim_sup())){ //Si el valor se encuentra dentro de los límites de la variable, no se le realiza cambio.
            valor_reparado = valor;
        }else if(valor < var.getLim_inf()){ //Si el valor se encuentra fuera de los límites
            while(!encontrado){
                valor_reparado = 2 * var.getLim_inf() - valor; //Se aplica fórmula de reparación
                if((valor_reparado >= var.getLim_inf()) && (valor_reparado <= var.getLim_sup())){ //Si el valor reparado ya se encuentra dentro del rango
                    encontrado = true;
                }
            }
        }else if(valor > var.getLim_sup()){ //Si el valor se encuentra fuera de los límites
            while(!encontrado){
                valor_reparado = 2 * var.getLim_sup() - valor; //Se aplica fórmula de reparación
                if((valor_reparado >= var.getLim_inf()) && (valor_reparado <= var.getLim_sup())){ //Si el valor reparado ya se encuentra dentro del rango
                    encontrado = true;
                }
            }
        }
        return valor_reparado;
    }
    
    //Recorro la población de padres para encontrar al mejor individuo
    public static void imprimirMejorIndividuo(int numGeneracion){
        double x = 0, y = 0, fx = 0, fx_mejor = 0;
        for (int i = 0; i < poblacionPadres.length; i++){
            fx = obtenerFuncion(poblacionPadres[i][0], poblacionPadres[i][1]);
            if((fx_mejor == 0) || (fx < fx_mejor)){ //Se trata del primer individuo evaluado o el fx encontrado es menor al mejor encontrado
                fx_mejor = fx; //Respaldo fx
                x = poblacionPadres[i][0]; //Respaldo el valor de x
                y = poblacionPadres[i][1]; //Respaldo el valor de y
            }
        }
        System.out.printf("Generación "+numGeneracion+". \tx = %.6f, y = %.6f, f(x,y) = %.6f \n", x, y, fx_mejor);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Genero mi población inicial
        for (int x = 0; x < poblacionPadres.length; x++) { //Lleno población inicial
            //Se listan las variables que se hayan definido
            poblacionPadres[x][0] = (Math.random() * (varX.getLim_sup() - varX.getLim_inf())) + varX.getLim_inf(); //Asigno valores entre los límites definidos para X 
            poblacionPadres[x][1] = (Math.random() * (varY.getLim_sup() - varY.getLim_inf())) + varY.getLim_inf(); //Asigno valores entre los límites definidos para Y
        }
        
        //Se recorre la población inicial para obtener e imprimir el mejor
        imprimirMejorIndividuo(1); 
        
        int encontrados;
        int indiceVector = 0;
        for (int g = 1; g < numGeneraciones; g++) {
            for (int i = 0; i < tamanioPoblacion; i++) { //Por cada target... 
                encontrados = 0;
                while (encontrados < 3) { //Selecciono 3 vectores diferentes al azar
                    indiceVector = (int) (Math.random() * (tamanioPoblacion - 0) + 0);
                    if (indiceVector != i) { //Que no sea el índice en el que estoy parado
                        vectoresSeleccionados[encontrados][0] = poblacionPadres[indiceVector][0]; //Primera variable
                        vectoresSeleccionados[encontrados][1] = poblacionPadres[indiceVector][1]; //Segunda variable
                        encontrados++;
                    }
                } //Hasta este punto ya tengo los 3 targets elegidos al azar.

                //Se genera el vector ruido (mutación)
                for (int j = 0; j < numVariables; j++) { //Por cada variable del vector
                    //Resto el valor del primer vector menos el valor del segundo vector, se multiplica por F y se suma el valor del tercer vector.
                    vectorRuido[0][j] = (vectoresSeleccionados[0][j] - vectoresSeleccionados[1][j]) * F + vectoresSeleccionados[2][j];
                }

                //Verificar y corregir el vector ruido (proceso de reparación)
                //Se listan las variables que se hayan definido
                vectorRuido[0][0] = repararVariable(varX, vectorRuido[0][0]); //Primera variable (X)
                vectorRuido[0][1] = repararVariable(varY, vectorRuido[0][1]); //Segunda variable(Y)

                //Recombinación (cruza)
                int jRand = (int)(Math.random() * (numVariables - 0) + 0);
                for (int j = 0; j < numVariables; j++) { //Para cada variable del vector 
                    if (((Math.random() * (1 - 0) + 0) < CR) || //Si el aleatorio entre 0 y 1, es menor a CR
                         j ==  jRand){  // o j es igual a un aleatorio
                        vectorTrial[0][j] = vectorRuido[0][j]; //El valor se toma del vector ruido
                    } else {
                        vectorTrial[0][j] = poblacionPadres[i][j]; //Si no, el valor se toma del vector target (padre)
                    }
                }

                //Evaluación 
                double fx_target = obtenerFuncion(poblacionPadres[i][0], poblacionPadres[i][1]);
                double fx_trail = obtenerFuncion(vectorTrial[0][0], vectorTrial[0][1]);

                //Selección
                if (fx_target < fx_trail) {
                    poblacionHijos[i][0] = poblacionPadres[i][0];
                    poblacionHijos[i][1] = poblacionPadres[i][1];
                } else {
                    poblacionHijos[i][0] = vectorTrial[0][0];
                    poblacionHijos[i][1] = vectorTrial[0][1];
                }

            }// Fin de por cada target...
                        
            //Reemplazo la población de padres antes de comenzar la siguiente iteración
            poblacionPadres = poblacionHijos; //Es determinística
            
            //Cuando tenga completa la población de hijos, se recorre y se imprime el mejor
            imprimirMejorIndividuo(g+1);
        }//Fin de for por numero de generaciones

    }
}
