/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia2_algoritmoediferencial;

/**
 *
 * @author Herminio
 */
public class Funciones {
    
    public double  f_Step(double Solucion[][]){
        double suma = 0;
        for(int i = 0; i < Solucion[0].length; i++){ //Recorro cada variable del arreglo de soluciones
            suma = suma + Math.pow(Math.floor(Solucion[0][i] + 0.5), 2);
        }
        return suma;
    } 
    
    public double  f_Schwefel(double Solucion[][]){
        double suma = 0, acomulado = 0;
        for(int i = 0; i < Solucion[0].length; i++){
            acomulado = 0;
            for(int j = 0; j <= i; j++){
                acomulado = acomulado + (Solucion[0][j]);
            }
            suma = suma + (Math.pow(acomulado, 2));
        }
        return suma;
    } 
    
    public double  f_Rastrigin(double Solucion[][]){
        double suma = 0;
        for(int i = 0; i < Solucion[0].length; i++){
            suma = suma + (Math.pow(Solucion[0][i],2) - (10 * Math.cos(2 * Math.PI * Solucion[0][i])) + 10);
        }
        return suma;
    } 
    
}
