/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia2_algoritmoediferencial;

/**
 *
 * @author Herminio
 */
public class Variable {
    public double lim_inf, lim_sup;
    
    public Variable(double lim_inf, double lim_sup){
        this.lim_inf = lim_inf;
        this.lim_sup = lim_sup;
    }

    public double getLim_inf() {
        return lim_inf;
    }

    public double getLim_sup() {
        return lim_sup;
    }
    
    
    
}
