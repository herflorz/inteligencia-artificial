/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia2_algoritmoediferencial;

/**
 *
 * @author Herminio
 */
public class IA2_AlgoritmoEDiferencial {
    public static int tamanioPoblacion = 10; //Debe ser un número par
    public static int numVariables = 10; // Se indica el número de variables
    public static int numGeneraciones = 15;  //Número de iteraciones
    public static double F = 0.5; //Mutación
    public static double CR = 0.7; //Cruza 

    public static Variable var1 = new Variable(-100.00, 100.00);  //Declaro las 10 variables del problema
    public static Variable var2 = new Variable(-100.00, 100.00); 
    public static Variable var3 = new Variable(-100.00, 100.00); 
    public static Variable var4 = new Variable(-100.00, 100.00); 
    public static Variable var5 = new Variable(-100.00, 100.00); 
    public static Variable var6 = new Variable(-100.00, 100.00); 
    public static Variable var7 = new Variable(-100.00, 100.00); 
    public static Variable var8 = new Variable(-100.00, 100.00); 
    public static Variable var9 = new Variable(-100.00, 100.00); 
    public static Variable var10 = new Variable(-100.00, 100.00); 
    
    public static double poblacionPadres[][] = new double[tamanioPoblacion][numVariables];
    public static double vectoresSeleccionados[][] = new double[3][numVariables];
    public static double vectorRuido[][] = new double[1][numVariables];
    public static double vectorTrial[][] = new double[1][numVariables];
    public static double poblacionHijos[][] = new double[tamanioPoblacion][numVariables];
    
    public static double Solucion[][] = new double[1][10]; //Arreglo en donde se guardarán los valores de la solución
    public static Funciones f = new Funciones(); //Instancia de la clase funciones
    
    public static double obtenerFuncion(double Solucion[][]){
        return f.f_Step(Solucion);
    }
    
    public static double repararVariable(Variable var, double valor) {
        boolean encontrado = false;
        double valor_reparado = 0;
        while (!encontrado) {
            if ((valor >= var.getLim_inf()) && (valor <= var.getLim_sup())) { //Si el valor se encuentra dentro de los límites de la variable, no se le realiza cambio.
                valor_reparado = valor;
                encontrado = true;
            } else if (valor < var.getLim_inf()) { //Si el valor se encuentra fuera de los límites
                valor_reparado = 2 * var.getLim_inf() - valor; //Se aplica fórmula de reparación
                if ((valor_reparado >= var.getLim_inf()) && (valor_reparado <= var.getLim_sup())) { //Si el valor reparado ya se encuentra dentro del rango
                    encontrado = true;
                } else {
                    valor = valor_reparado;
                }
            } else if (valor > var.getLim_sup()) { //Si el valor se encuentra fuera de los límites
                valor_reparado = 2 * var.getLim_sup() - valor; //Se aplica fórmula de reparación
                if ((valor_reparado >= var.getLim_inf()) && (valor_reparado <= var.getLim_sup())) { //Si el valor reparado ya se encuentra dentro del rango
                    encontrado = true;
                } else {
                    valor = valor_reparado;
                }
            }
        }
        return valor_reparado;
    }
    
    //Recorro la población de padres para encontrar al mejor individuo
    public static void imprimirMejorIndividuo(int numGeneracion){
        double x1 = 0, x2 = 0, x3 = 0, x4 = 0, x5 = 0, x6 = 0, x7 = 0, x8 = 0, x9 = 0, x10 = 0, fx = 0, fx_mejor = 0;
        for (int i = 0; i < poblacionPadres.length; i++){
            Solucion[0][0] = poblacionPadres[i][0];
            Solucion[0][1] = poblacionPadres[i][1];
            Solucion[0][2] = poblacionPadres[i][2];
            Solucion[0][3] = poblacionPadres[i][3];
            Solucion[0][4] = poblacionPadres[i][4];
            Solucion[0][5] = poblacionPadres[i][5];
            Solucion[0][6] = poblacionPadres[i][6];
            Solucion[0][7] = poblacionPadres[i][7];
            Solucion[0][8] = poblacionPadres[i][8];
            Solucion[0][9] = poblacionPadres[i][9];
            fx = obtenerFuncion(Solucion);
            if((fx_mejor == 0) || (fx < fx_mejor)){ //Se trata del primer individuo evaluado o el fx encontrado es menor al mejor encontrado
                fx_mejor = fx; //Respaldo fx
                x1 = poblacionPadres[i][0]; 
                x2 = poblacionPadres[i][1]; 
                x3 = poblacionPadres[i][2]; 
                x4 = poblacionPadres[i][3]; 
                x5 = poblacionPadres[i][4]; 
                x6 = poblacionPadres[i][5]; 
                x7 = poblacionPadres[i][6]; 
                x8 = poblacionPadres[i][7]; 
                x9 = poblacionPadres[i][8]; 
                x10 = poblacionPadres[i][9]; 
            }
        }
        System.out.printf("Generación "+numGeneracion+". \t%.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, f = %.6f \n", x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, fx_mejor);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Genero mi población inicial
        for (int x = 0; x < poblacionPadres.length; x++) { //Lleno población inicial
            //Se listan las variables que se hayan definido
            poblacionPadres[x][0] = (Math.random() * (var1.getLim_sup() - var1.getLim_inf())) + var1.getLim_inf(); 
            poblacionPadres[x][1] = (Math.random() * (var2.getLim_sup() - var2.getLim_inf())) + var2.getLim_inf(); 
            poblacionPadres[x][2] = (Math.random() * (var3.getLim_sup() - var3.getLim_inf())) + var3.getLim_inf(); 
            poblacionPadres[x][3] = (Math.random() * (var4.getLim_sup() - var4.getLim_inf())) + var4.getLim_inf(); 
            poblacionPadres[x][4] = (Math.random() * (var5.getLim_sup() - var5.getLim_inf())) + var5.getLim_inf(); 
            poblacionPadres[x][5] = (Math.random() * (var6.getLim_sup() - var6.getLim_inf())) + var6.getLim_inf(); 
            poblacionPadres[x][6] = (Math.random() * (var7.getLim_sup() - var7.getLim_inf())) + var7.getLim_inf(); 
            poblacionPadres[x][7] = (Math.random() * (var8.getLim_sup() - var8.getLim_inf())) + var8.getLim_inf(); 
            poblacionPadres[x][8] = (Math.random() * (var9.getLim_sup() - var9.getLim_inf())) + var9.getLim_inf(); 
            poblacionPadres[x][9] = (Math.random() * (var10.getLim_sup() - var10.getLim_inf())) + var10.getLim_inf(); 
        }
        
        //Se recorre la población inicial para obtener e imprimir el mejor
        imprimirMejorIndividuo(1); 
        
        int encontrados;
        int indiceVector = 0;
        for (int g = 1; g < numGeneraciones; g++) {
            for (int i = 0; i < tamanioPoblacion; i++) { //Por cada target... 
                encontrados = 0;
                while (encontrados < 3) { //Selecciono 3 vectores diferentes al azar
                    indiceVector = (int) (Math.random() * (tamanioPoblacion - 0) + 0);
                    if (indiceVector != i) { //Que no sea el índice en el que estoy parado
                        vectoresSeleccionados[encontrados][0] = poblacionPadres[indiceVector][0]; //Primera variable
                        vectoresSeleccionados[encontrados][1] = poblacionPadres[indiceVector][1]; //Segunda variable
                        encontrados++;
                    }
                } //Hasta este punto ya tengo los 3 targets elegidos al azar.

                //Se genera el vector ruido (mutación)
                for (int j = 0; j < numVariables; j++) { //Por cada variable del vector
                    //Resto el valor del primer vector menos el valor del segundo vector, se multiplica por F y se suma el valor del tercer vector.
                    vectorRuido[0][j] = (vectoresSeleccionados[0][j] - vectoresSeleccionados[1][j]) * F + vectoresSeleccionados[2][j];
                }

                //Verificar y corregir el vector ruido (proceso de reparación)
                //Se listan las variables que se hayan definido
                vectorRuido[0][0] = repararVariable(var1, vectorRuido[0][0]);
                vectorRuido[0][1] = repararVariable(var2, vectorRuido[0][1]); 
                vectorRuido[0][2] = repararVariable(var3, vectorRuido[0][2]); 
                vectorRuido[0][3] = repararVariable(var4, vectorRuido[0][3]); 
                vectorRuido[0][4] = repararVariable(var5, vectorRuido[0][4]); 
                vectorRuido[0][5] = repararVariable(var6, vectorRuido[0][5]); 
                vectorRuido[0][6] = repararVariable(var7, vectorRuido[0][6]); 
                vectorRuido[0][7] = repararVariable(var8, vectorRuido[0][7]); 
                vectorRuido[0][8] = repararVariable(var9, vectorRuido[0][8]); 
                vectorRuido[0][9] = repararVariable(var10, vectorRuido[0][9]); 

                //Recombinación (cruza)
                int jRand = (int)(Math.random() * (numVariables - 0) + 0);
                for (int j = 0; j < numVariables; j++) { //Para cada variable del vector 
                    if (((Math.random() * (1 - 0) + 0) < CR) || //Si el aleatorio entre 0 y 1, es menor a CR
                         j ==  jRand){  // o j es igual a un aleatorio
                        vectorTrial[0][j] = vectorRuido[0][j]; //El valor se toma del vector ruido
                    } else {
                        vectorTrial[0][j] = poblacionPadres[i][j]; //Si no, el valor se toma del vector target (padre)
                    }
                }

                //Evaluación 
                Solucion[0][0] = poblacionPadres[i][0];
                Solucion[0][1] = poblacionPadres[i][1];
                Solucion[0][2] = poblacionPadres[i][2];
                Solucion[0][3] = poblacionPadres[i][3];
                Solucion[0][4] = poblacionPadres[i][4];
                Solucion[0][5] = poblacionPadres[i][5];
                Solucion[0][6] = poblacionPadres[i][6];
                Solucion[0][7] = poblacionPadres[i][7];
                Solucion[0][8] = poblacionPadres[i][8];
                Solucion[0][9] = poblacionPadres[i][9];
                double fx_target = obtenerFuncion(Solucion);
                Solucion[0][0] = vectorTrial[0][0];
                Solucion[0][1] = vectorTrial[0][1];
                Solucion[0][2] = vectorTrial[0][2];
                Solucion[0][3] = vectorTrial[0][3];
                Solucion[0][4] = vectorTrial[0][4];
                Solucion[0][5] = vectorTrial[0][5];
                Solucion[0][6] = vectorTrial[0][6];
                Solucion[0][7] = vectorTrial[0][7];
                Solucion[0][8] = vectorTrial[0][8];
                Solucion[0][9] = vectorTrial[0][9];
                double fx_trail = obtenerFuncion(Solucion);

                //Selección
                if (fx_target < fx_trail) {
                    poblacionHijos[i][0] = poblacionPadres[i][0];
                    poblacionHijos[i][1] = poblacionPadres[i][1];
                } else {
                    poblacionHijos[i][0] = vectorTrial[0][0];
                    poblacionHijos[i][1] = vectorTrial[0][1];
                }

            }// Fin de por cada target...
                        
            //Reemplazo la población de padres antes de comenzar la siguiente iteración
            poblacionPadres = poblacionHijos; //Es determinística
            
            //Cuando tenga completa la población de hijos, se recorre y se imprime el mejor
            imprimirMejorIndividuo(g+1);
        }//Fin de for por numero de generaciones

    }
}
