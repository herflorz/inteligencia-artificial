/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia2_6algoritmopso;

/**
 *
 * @author Herminio
 */
public class IA2_6AlgoritmoPSO {

    public static int tamanioPoblacion = 1000; //Debe ser un número par
    public static int numVariables = 10; // Se indica el número de variables
    public static int numGeneraciones = 1000; //Número de iteraciones
    public static double W = 0.4;  //Valor entre 0.1 y 0.5, peso de inercia
    public static double c1 = 1.60; //Valor entre 1.5 y 2.0, cognitivo
    public static double c2 = 2.00; //Valor entre 1.5 y 2.0, social
    public static double pMutacion = 0.4; //Probabilidad de mutación, entre 0 y 1

    public static Variable var1 = new Variable(-5.12, 5.12);  //Declaro las 10 variables del problema
    public static Variable var2 = new Variable(-5.12, 5.12); 
    public static Variable var3 = new Variable(-5.12, 5.12); 
    public static Variable var4 = new Variable(-5.12, 5.12); 
    public static Variable var5 = new Variable(-5.12, 5.12); 
    public static Variable var6 = new Variable(-5.12, 5.12); 
    public static Variable var7 = new Variable(-5.12, 5.12); 
    public static Variable var8 = new Variable(-5.12, 5.12); 
    public static Variable var9 = new Variable(-5.12, 5.12); 
    public static Variable var10 = new Variable(-5.12, 5.12); 

    public static double swarm_actual[][] = new double[tamanioPoblacion][numVariables];
    public static double velocidad[][] = new double[tamanioPoblacion][numVariables];
    public static double pbest[][] = new double[tamanioPoblacion][numVariables];
    public static double gbest[][] = new double[1][numVariables];
    
    public static double Solucion[][] = new double[1][10]; //Arreglo en donde se guardarán los valores de la solución
    public static Funciones f = new Funciones(); //Instancia de la clase funciones

    public static double obtenerFuncion(double Solucion[][]) {
        return f.f_Rastrigin(Solucion);
    }

    //Recorro el pbest para encontrar al mejor individuo
    public static void actualizarGBest(int numGeneracion) {
        double x1 = 0, x2 = 0, x3 = 0, x4 = 0, x5 = 0, x6 = 0, x7 = 0, x8 = 0, x9 = 0, x10 = 0, fx = 0, fx_mejor = 0; //swarm_actual
        for (int i = 0; i < swarm_actual.length; i++) { //Recorro cada partícula del swarm
            Solucion[0][0] = swarm_actual[i][0];
            Solucion[0][1] = swarm_actual[i][1];
            Solucion[0][2] = swarm_actual[i][2];
            Solucion[0][3] = swarm_actual[i][3];
            Solucion[0][4] = swarm_actual[i][4];
            Solucion[0][5] = swarm_actual[i][5];
            Solucion[0][6] = swarm_actual[i][6];
            Solucion[0][7] = swarm_actual[i][7];
            Solucion[0][8] = swarm_actual[i][8];
            Solucion[0][9] = swarm_actual[i][9];
            fx = obtenerFuncion(Solucion);
            if ((fx_mejor == 0) || (fx < fx_mejor)) { //Se trata del primer individuo evaluado o el fx encontrado es menor al mejor encontrado
                fx_mejor = fx; //Respaldo fx
                x1 = swarm_actual[i][0]; 
                x2 = swarm_actual[i][1]; 
                x3 = swarm_actual[i][2]; 
                x4 = swarm_actual[i][3]; 
                x5 = swarm_actual[i][4]; 
                x6 = swarm_actual[i][5]; 
                x7 = swarm_actual[i][6]; 
                x8 = swarm_actual[i][7];
                x9 = swarm_actual[i][8]; 
                x10 = swarm_actual[i][9];
            }
        }
        
        //Lleno el vector gbest
        Solucion[0][0] = gbest[0][0];
        Solucion[0][1] = gbest[0][1];
        Solucion[0][2] = gbest[0][2];
        Solucion[0][3] = gbest[0][3];
        Solucion[0][4] = gbest[0][4];
        Solucion[0][5] = gbest[0][5];
        Solucion[0][6] = gbest[0][6];
        Solucion[0][7] = gbest[0][7];
        Solucion[0][8] = gbest[0][8];
        Solucion[0][9] = gbest[0][9];
        if(numGeneracion==1 || fx_mejor<obtenerFuncion(Solucion)){
            gbest[0][0] = x1;
            gbest[0][1] = x2;
            gbest[0][2] = x3;
            gbest[0][3] = x4;
            gbest[0][4] = x5;
            gbest[0][5] = x6;
            gbest[0][6] = x7;
            gbest[0][7] = x8;
            gbest[0][8] = x9;
            gbest[0][9] = x10;
        }   
    }

    public static double repararVariable(Variable var, double valor) {
        boolean encontrado = false;
        double valor_reparado = 0;
        while (!encontrado) {
            if ((valor >= var.getLim_inf()) && (valor <= var.getLim_sup())) { //Si el valor se encuentra dentro de los límites de la variable, no se le realiza cambio.
                valor_reparado = valor;
                encontrado = true;
            } else if (valor < var.getLim_inf()) { //Si el valor se encuentra fuera de los límites
                valor_reparado = 2 * var.getLim_inf() - valor; //Se aplica fórmula de reparación
                if ((valor_reparado >= var.getLim_inf()) && (valor_reparado <= var.getLim_sup())) { //Si el valor reparado ya se encuentra dentro del rango
                    encontrado = true;
                } else {
                    valor = valor_reparado;
                }
            } else if (valor > var.getLim_sup()) { //Si el valor se encuentra fuera de los límites
                valor_reparado = 2 * var.getLim_sup() - valor; //Se aplica fórmula de reparación
                if ((valor_reparado >= var.getLim_inf()) && (valor_reparado <= var.getLim_sup())) { //Si el valor reparado ya se encuentra dentro del rango
                    encontrado = true;
                } else {
                    valor = valor_reparado;
                }
            }
        }
        return valor_reparado;
    }
    
    //La mejor solución siempre se encuentra en pbest
    public static void imprimirMejorSolucion(int numGeneracion){
        double x1 = 0, x2 = 0, x3 = 0, x4 = 0, x5 = 0, x6 = 0, x7 = 0, x8 = 0, x9 = 0, x10 = 0, fx = 0, fx_mejor = 0; //swarm_actual
        for (int i = 0; i < pbest.length; i++) { //Recorro cada partícula del swarm
            Solucion[0][0] = pbest[i][0];
            Solucion[0][1] = pbest[i][1];
            Solucion[0][2] = pbest[i][2];
            Solucion[0][3] = pbest[i][3];
            Solucion[0][4] = pbest[i][4];
            Solucion[0][5] = pbest[i][5];
            Solucion[0][6] = pbest[i][6];
            Solucion[0][7] = pbest[i][7];
            Solucion[0][8] = pbest[i][8];
            Solucion[0][9] = pbest[i][9];
            fx = obtenerFuncion(Solucion);
            if ((fx_mejor == 0) || (fx < fx_mejor)) { //Se trata del primer individuo evaluado o el fx encontrado es menor al mejor encontrado
                fx_mejor = fx; //Respaldo fx
                x1 = pbest[i][0]; 
                x2 = pbest[i][1]; 
                x3 = pbest[i][2];
                x4 = pbest[i][3];
                x5 = pbest[i][4];
                x6 = pbest[i][5];
                x7 = pbest[i][6];
                x8 = pbest[i][7];
                x9 = pbest[i][8];
                x10 = pbest[i][9];
            }
        }
        System.out.printf("Generación "+numGeneracion+". \t%.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, f = %.6f \n", x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, fx_mejor);
        Solucion[0][0] = gbest[0][0];
        Solucion[0][1] = gbest[0][1];
        Solucion[0][2] = gbest[0][2];
        Solucion[0][3] = gbest[0][3];
        Solucion[0][4] = gbest[0][4];
        Solucion[0][5] = gbest[0][5];
        Solucion[0][6] = gbest[0][6];
        Solucion[0][7] = gbest[0][7];
        Solucion[0][8] = gbest[0][8];
        Solucion[0][9] = gbest[0][9];
        System.out.printf("%.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, f = %.6f \n", gbest[0][0], gbest[0][1], gbest[0][2], gbest[0][3], gbest[0][4], gbest[0][5], gbest[0][6], gbest[0][7], gbest[0][8], gbest[0][9], obtenerFuncion(Solucion));
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double r1, r2;
        //Generar cúmulo de partículas inicial de soluciones
        for (int x = 0; x < swarm_actual.length; x++) { //Lleno cúmulo inicial
            //Se listan las variables que se hayan definido
            swarm_actual[x][0] = (Math.random() * (var1.getLim_sup() - var1.getLim_inf())) + var1.getLim_inf(); 
            swarm_actual[x][1] = (Math.random() * (var2.getLim_sup() - var2.getLim_inf())) + var2.getLim_inf();
            swarm_actual[x][2] = (Math.random() * (var3.getLim_sup() - var3.getLim_inf())) + var3.getLim_inf();
            swarm_actual[x][3] = (Math.random() * (var4.getLim_sup() - var4.getLim_inf())) + var4.getLim_inf();
            swarm_actual[x][4] = (Math.random() * (var5.getLim_sup() - var5.getLim_inf())) + var5.getLim_inf();
            swarm_actual[x][5] = (Math.random() * (var6.getLim_sup() - var6.getLim_inf())) + var6.getLim_inf();
            swarm_actual[x][6] = (Math.random() * (var7.getLim_sup() - var7.getLim_inf())) + var7.getLim_inf();
            swarm_actual[x][7] = (Math.random() * (var8.getLim_sup() - var8.getLim_inf())) + var8.getLim_inf();
            swarm_actual[x][8] = (Math.random() * (var9.getLim_sup() - var9.getLim_inf())) + var9.getLim_inf();
            swarm_actual[x][9] = (Math.random() * (var10.getLim_sup() - var10.getLim_inf())) + var10.getLim_inf();
        }
        
        //Inicializo el vector pbest 
        //Como es la primera iteración, el pbest es igual al swarm
        for (int x = 0; x < swarm_actual.length; x++) {
            for(int y = 0; y < swarm_actual[x].length; y++){
                pbest[x][y] = swarm_actual[x][y];
            }
        }

        //Inicializo el vector de velocidad
        for (int x = 0; x < velocidad.length; x++) {
            for (int y = 0; y < velocidad[x].length; y++) {
                velocidad[x][y] = 0.0; //Se inicializa el vector de velocidad en 0 
            }
        }
        
        //Localizar al gbest
        actualizarGBest(1);
        
        //Imprimir mejor solución
        imprimirMejorSolucion(1);

        for (int g = 1; g < numGeneraciones; g++) { //Por cada generación
            //Recorro las partículas del swarm (cada individuo de la población)
            for (int i = 0; i < swarm_actual.length; i++) {
                //Genero varoles aleatorios para cada individuo
//                W = (Math.random() * (0.5 - 0.1) + 0.1);  //Valor entre 0.1 y 0.5, peso de inercia
//                c1 = (Math.random() * (2.0 - 1.5) + 1.5); //Valor entre 1.5 y 2.0, cognitivo
//                c2 = (Math.random() * (2.0 - 1.5) + 1.5); //Valor entre 1.5 y 2.0, social
                r1 = (Math.random() * (1.0 - 0.0) + 0.0); //Aleatorio entre 0.0 y 1.0
                r2 = (Math.random() * (1.0 - 0.0) + 0.0); //Aleatorio entre 0.0 y 1.0
                //Actualizo el vector de velocidad
                for (int j = 0; j < swarm_actual[i].length; j++) {  
                    double inercia = W * velocidad[i][j];
                    double experiencia_cognitiva = c1 * r1 * (pbest[i][j] - swarm_actual[i][j]);
                    double experiencia_social = c2 * r2 * (gbest[0][j] - swarm_actual[i][j]);
                    velocidad[i][j] = inercia + experiencia_cognitiva + experiencia_social;
                    //Actualizo la posición actual
                    swarm_actual[i][j] = swarm_actual[i][j] + velocidad[i][j];
                }
                
                //Verificar y corregir el vector swarm (proceso de reparación)
                //Se listan las variables que se hayan definido,
                swarm_actual[i][0] = repararVariable(var1, swarm_actual[i][0]); 
                swarm_actual[i][1] = repararVariable(var2, swarm_actual[i][1]); 
                swarm_actual[i][2] = repararVariable(var3, swarm_actual[i][2]); 
                swarm_actual[i][3] = repararVariable(var4, swarm_actual[i][3]); 
                swarm_actual[i][4] = repararVariable(var5, swarm_actual[i][4]); 
                swarm_actual[i][5] = repararVariable(var6, swarm_actual[i][5]); 
                swarm_actual[i][6] = repararVariable(var7, swarm_actual[i][6]); 
                swarm_actual[i][7] = repararVariable(var8, swarm_actual[i][7]); 
                swarm_actual[i][8] = repararVariable(var9, swarm_actual[i][8]); 
                swarm_actual[i][9] = repararVariable(var10, swarm_actual[i][9]); 

                //Operador de turbulencia (mutación)
                if ((Math.random() * (1 - 0) + 0) < pMutacion) { //Si un número aleatorio es menor al parámetro de mutación
                    int indice = (int) (Math.random() * (numVariables - 0) + 0); //Elijo al azar el índice de la variable que se va a cambiar
                    if (indice == 0) {
                        swarm_actual[i][indice] = (Math.random() * (var1.getLim_sup() - var1.getLim_inf())) + var1.getLim_inf(); //Se asumen que todas las variables tienen los mismos límites
                    } 
                }
                
                //Evaluación
                Solucion[0][0] = pbest[i][0];
                Solucion[0][1] = pbest[i][1];
                Solucion[0][2] = pbest[i][2];
                Solucion[0][3] = pbest[i][3];
                Solucion[0][4] = pbest[i][4];
                Solucion[0][5] = pbest[i][5];
                Solucion[0][6] = pbest[i][6];
                Solucion[0][7] = pbest[i][7];
                Solucion[0][8] = pbest[i][8];
                Solucion[0][9] = pbest[i][9];
                double fx_pbest = obtenerFuncion(Solucion);
                Solucion[0][0] = swarm_actual[i][0];
                Solucion[0][1] = swarm_actual[i][1];
                Solucion[0][2] = swarm_actual[i][2];
                Solucion[0][3] = swarm_actual[i][3];
                Solucion[0][4] = swarm_actual[i][4];
                Solucion[0][5] = swarm_actual[i][5];
                Solucion[0][6] = swarm_actual[i][6];
                Solucion[0][7] = swarm_actual[i][7];
                Solucion[0][8] = swarm_actual[i][8];
                Solucion[0][9] = swarm_actual[i][9];
                double fx_actual = obtenerFuncion(Solucion);

                //Actualización del pbest
                if (fx_actual < fx_pbest) { //Si la solución anterior era mejor a la actual, rescato la solución anterior
                    pbest[i][0] = swarm_actual[i][0]; //Variable X
                    pbest[i][1] = swarm_actual[i][1]; //Variable Y
                    pbest[i][2] = swarm_actual[i][2]; //Variable Y
                    pbest[i][3] = swarm_actual[i][3]; //Variable Y
                    pbest[i][4] = swarm_actual[i][4]; //Variable Y
                    pbest[i][5] = swarm_actual[i][5]; //Variable Y
                    pbest[i][6] = swarm_actual[i][6]; //Variable Y
                    pbest[i][7] = swarm_actual[i][7]; //Variable Y
                    pbest[i][8] = swarm_actual[i][8]; //Variable Y
                    pbest[i][9] = swarm_actual[i][9]; //Variable Y
                } //Si no, pbest no se afecta
            }
                
            //Actualizo al gbest
            actualizarGBest(g + 1);
            
            //Imprimir mejor solución
            imprimirMejorSolucion(g + 1);
        } //Finaliza el for de las generaciones
    }
}

