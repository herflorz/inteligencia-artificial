/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia2_proynreinas;

/**
 *
 * @author Herminio
 */
public class IA2_ProyNReinas {
    public static int topeIteraciones = 100000; //Indica el número de paro de iteraciones
    public static Tablero tp1=null, tp2=null, tp3=null, tp4=null, tp5=null, tp6=null; //Tableros padres
    public static Tablero ts1=null, ts2=null, ts3=null, ts4=null, ts5=null, ts6=null; //Tableros seleccionados
    public static Tablero th1=null, th2=null, th3=null, th4=null, th5=null, th6=null; //Tableros hijos
    public static int fcal1, fcal2, fcal3, fcal4, fcal5, fcal6;  //Guarda la función de calidad de cada tablero
    
    public static Tablero seleccionarTablero(){
        Tablero tableroGanador = null, tableroPadre1 = null, tableroPadre2 = null;
        boolean sonDiferentes = false;
        //Genero un número al azar entre 1 y 6 para poder seleccionar un tablero
        int azar = (int) (Math.random() * (6 - 1)) + 1;
        switch (azar) {
            case 1: 
                tableroPadre1= tp1;
                break;
            case 2: 
                tableroPadre1= tp2; 
                break;
            case 3: 
                tableroPadre1= tp3; 
                break;
            case 4: 
                tableroPadre1= tp4;
                break;
            case 5:
                tableroPadre1= tp5;
                break;
            case 6:
                tableroPadre1= tp6;
                break;
        }
        while (!sonDiferentes) {
            azar = (int) (Math.random() * (6 - 1)) + 1;
            switch (azar) {
                case 1:
                    tableroPadre2 = tp1;
                    break;
                case 2:
                    tableroPadre2 = tp2;
                    break;
                case 3:
                    tableroPadre2 = tp3;
                    break;
                case 4:
                    tableroPadre2 = tp4;
                    break;
                case 5:
                    tableroPadre2 = tp5;
                    break;
                case 6:
                    tableroPadre2 = tp6;
                    break;
            }
            if((!tableroPadre1.equals(tableroPadre2)) || //Si los tableros son diferentes
                    (ts1!=null && ts1!=tableroPadre1 && ts1!=tableroPadre2) || //Valido que no se seleccionen tableros duplicados/repetidos
                    (ts2!=null && ts2!=tableroPadre1 && ts2!=tableroPadre2) ||
                    (ts3!=null && ts3!=tableroPadre1 && ts3!=tableroPadre2) ||
                    (ts4!=null && ts4!=tableroPadre1 && ts4!=tableroPadre2) ||
                    (ts5!=null && ts5!=tableroPadre1 && ts5!=tableroPadre2) ||
                    (ts6!=null && ts6!=tableroPadre1 && ts6!=tableroPadre2)){
                sonDiferentes = true;
            }
        }//Fin de while
        if(tableroPadre1.obtenerFuncionCalidad()<tableroPadre2.obtenerFuncionCalidad()){
            tableroGanador = tableroPadre1;
        }else if(tableroPadre1.obtenerFuncionCalidad()>tableroPadre2.obtenerFuncionCalidad()){
            tableroGanador = tableroPadre2;
        }else{
            tableroGanador = tableroPadre1; //Es el caso cuando los dos tableros tienen igual función de calidad
        }
        return tableroGanador;
    }
    
    public static void main(String[] args) {
        // 1. Representación de soluciones   // 3. Población de soluciones
        tp1 = new Tablero(1);
        tp2 = new Tablero(2);
        tp3 = new Tablero(3);
        tp4 = new Tablero(4);
        tp5 = new Tablero(5);
        tp6 = new Tablero(6);
        
        // 2. Función de calidad, definida por el número de ataques
        System.out.println("\n\nFunción de calidad para tablero 1 = "+tp1.obtenerFuncionCalidad());
        System.out.println("Función de calidad para tablero 2 = "+tp2.obtenerFuncionCalidad());
        System.out.println("Función de calidad para tablero 3 = "+tp3.obtenerFuncionCalidad());
        System.out.println("Función de calidad para tablero 4 = "+tp4.obtenerFuncionCalidad());
        System.out.println("Función de calidad para tablero 5 = "+tp5.obtenerFuncionCalidad());
        System.out.println("Función de calidad para tablero 6 = "+tp6.obtenerFuncionCalidad());
        
        int contadorIteraciones = 0;
        while ((contadorIteraciones <= topeIteraciones) || (fcal1==0) || (fcal2==0) || (fcal3==0) || (fcal4==0) || (fcal5==0) || (fcal6==0)) {
            // 4. Mecanismo de selección de padres
            ts1 = seleccionarTablero();
            ts2 = seleccionarTablero();
            ts3 = seleccionarTablero();
            ts4 = seleccionarTablero();
            ts5 = seleccionarTablero();
            ts6 = seleccionarTablero();

            // 5. Operadores de variación (cruza y/o mutación)
            th1 = new Tablero(1, ts1, ts2); //Con el padre seleccionado 1 y 2 genero un hijo
            th1.verificarMutacion();
            th2 = new Tablero(2, ts3, ts4); //Con el padre seleccionado 3 y 4 genero un hijo
            th2.verificarMutacion();
            th3 = new Tablero(3, ts5, ts6); //Con el padre seleccionado 5 y 6 genero un hijo
            th3.verificarMutacion();
            th4 = new Tablero(4, ts1, ts4); //Con el padre seleccionado 1 y 4 genero un hijo
            th4.verificarMutacion();
            th5 = new Tablero(5, ts3, ts6); //Con el padre seleccionado 3 y 6 genero un hijo
            th5.verificarMutacion();
            th6 = new Tablero(6, ts2, ts5); //Con el padre seleccionado 2 y 5 genero un hijo
            th6.verificarMutacion();

            // 6. Mecanismo de reemplazo.- Los hijos siempre reemplazan a los padres
            tp1 = th1;
            tp2 = th2;
            tp3 = th3;
            tp4 = th4;
            tp5 = th5;
            tp6 = th6;
            
            //Obtengo la función de calidad
            fcal1 = tp1.obtenerFuncionCalidad();
            fcal2 = tp2.obtenerFuncionCalidad();
            fcal3 = tp3.obtenerFuncionCalidad();
            fcal4 = tp4.obtenerFuncionCalidad();
            fcal5 = tp5.obtenerFuncionCalidad();
            fcal6 = tp6.obtenerFuncionCalidad();
                    
            contadorIteraciones++; //Incremento el número de ireaciones.
        }//Fin del while
    

        //Imprimo los resultados obtenidos: Los tableros y las funciones de calidad de cada uno.
        tp1.imprimirTablero();
        tp2.imprimirTablero();
        tp3.imprimirTablero();
        tp4.imprimirTablero();
        tp5.imprimirTablero();
        tp6.imprimirTablero();
        
        System.out.println("\n\nFunción de calidad para tablero 1 = "+fcal1);
        System.out.println("Función de calidad para tablero 2 = "+fcal2);
        System.out.println("Función de calidad para tablero 3 = "+fcal3);
        System.out.println("Función de calidad para tablero 4 = "+fcal4);
        System.out.println("Función de calidad para tablero 5 = "+fcal5);
        System.out.println("Función de calidad para tablero 6 = "+fcal6);
    }
    
}
