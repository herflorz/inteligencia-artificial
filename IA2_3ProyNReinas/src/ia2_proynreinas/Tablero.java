package ia2_proynreinas;


/**
 *
 * @author Herminio
 */
public class Tablero {
    String tablero[][] = null;
    int identificador = 0;
    
    /**
     * Constructor que se usa para generar la población inicial de tableros
     * @param identificador: recibe un número para identificar los tableros 
     */
    public Tablero(int identificador){
        int posX=0, posY=0;
        this.identificador = identificador;
        this.tablero = new String[8][8]; //Defino el tablero de 8 x 8
        //Lleno el tablero con vacíos
        for (int x=0; x< this.tablero.length; x++){
            for (int y = 0; y < this.tablero[x].length; y++){
                this.tablero[x][y] = ".";
            }
        }
        //Genero posiciones al azar para llenar el tablero
        int numReinas = 1;
        while(numReinas<=8){ //Hasta que coloque las 8 reinas que requiere el problema
            posX = (int) Math.floor(Math.random()*7);
            posY = (int) Math.floor(Math.random()*7);
            if(this.tablero[posX][posY].equals(".")){
               this.tablero[posX][posY] = "R";
               numReinas++; 
            }
        }
        //Imprimo la el individuo inicial generado.
        imprimirTablero();
    }
    
    /**
     * Constructor que se usa para cruzar dos padres y generar un nuevo hijo con 4 reinas del ts1 y 4 reinas de ts2
     * @param identificador: recibe un número para identificar los tableros 
     * @param ts1: recibe el tablero padre 1
     * @param ts2: recibe el tablero padre 2
     */
    public Tablero(int identificador, Tablero ts1, Tablero ts2){
        this.identificador = identificador;
        this.tablero = new String[8][8]; //Defino el tablero de 8 x 8
        //Lleno el tablero con vacíos
        for (int x=0; x< this.tablero.length; x++){
            for (int y = 0; y < this.tablero[x].length; y++){
                this.tablero[x][y] = ".";
            }
        }
        int contador=0;
        while (contador < 4) { //Saco los primeros 4 del primer tablero seleccionado
            for (int x = 0; x < ts1.tablero.length; x++) {
                for (int y = 0; y < ts1.tablero[x].length; y++) {
                    if ((ts1.tablero[x][y].equals("R")) && (this.tablero[x][y].equals("."))) {
                        this.tablero[x][y] = "R";
                        contador++;
                        if (contador == 4) {
                            break;
                        }
                    }

                }
                if (contador == 4) {
                    break;
                }
            }
        }
        contador = 0;
        while (contador < 4) { //Saco los últimos 4 del segundo tablero seleccionado
            for (int x = ts2.tablero.length - 1; x >= 0; x--) {
                for (int y = ts2.tablero[x].length - 1; y >= 0; y--) {
                    if ((ts2.tablero[x][y].equals("R")) && (this.tablero[x][y].equals("."))) {
                        this.tablero[x][y] = "R";
                        contador++;
                        if (contador == 4) {
                            break;
                        }
                    }

                }
                if (contador == 4) {
                    break;
                }
            }
        }
    }
    
    /**
     * Función para imprimir el tablero de 8 x 8, indicando en dónde se encuentran posicionadas las 8 reinas
     */
    public void imprimirTablero(){
        System.out.print("\n      < Tablero "+this.identificador+" >\n"); //Limpio la pantalla, para imprimir la nueva posición del agente
        for(int x=0; x < this.tablero.length; x++) {
            for (int y=0; y < this.tablero[x].length; y++) {
                System.out.printf("%3s", this.tablero[x][y]);
            }
        System.out.println("");  //Fin  
        }   
    }
    
    /**
     * Función que obtiene la función de calidad, es decir, el número de ataques que se encontraron en el tablero
     * Una reina amenaza a otra si está en la misma fila, la misma columna o en la misma diagonal.
     * A menor cantidad mayor es la calidad
     * @return: Cantidad de ataques que tiene el tablero
     */
    public int obtenerFuncionCalidad(){
        int cantidad = 0, auxX = 0, auxY = 0; 
        boolean encontrado = false;
        for (int x=0; x< this.tablero.length; x++){
            for (int y = 0; y < this.tablero[x].length; y++){
                if(this.tablero[x][y].equals("R")){ //Se trata de una reina, entonces evaluamos
                    //Evaluamos en vertical x
                    auxX = x-1;
                    auxY = y;
                    encontrado = false;
                    while((!encontrado) && (auxX>=0 && auxX<this.tablero.length) && (auxY>=0 && auxY<this.tablero[0].length)){
                        if(this.tablero[auxX--][auxY].equals("R")){
                            cantidad++;
                            encontrado = true;
                        }
                    }
                    auxX = x+1;
                    auxY = y;
                    encontrado = false;
                    while((!encontrado) && (auxX>=0 && auxX<this.tablero.length) && (auxY>=0 && auxY<this.tablero[0].length)){
                        if(this.tablero[auxX++][auxY].equals("R")){
                            cantidad++;
                            encontrado = true;
                        }
                    }
                    //Evaluamos en horizontal y
                    auxX = x;
                    auxY = y-1;
                    encontrado = false;
                    while((!encontrado) && (auxX>=0 && auxX<this.tablero.length) && (auxY>=0 && auxY<this.tablero[0].length)){
                        if(this.tablero[auxX][auxY--].equals("R")){
                            cantidad++;
                            encontrado = true;
                        }
                    }
                    auxX = x;
                    auxY = y+1;
                    encontrado = false;
                    while((!encontrado) && (auxX>=0 && auxX<this.tablero.length) && (auxY>=0 && auxY<this.tablero[0].length)){
                        if(this.tablero[auxX][auxY++].equals("R")){
                            cantidad++;
                            encontrado = true;
                        }
                    }
                    //Evaluamos en diagonal /
                    auxX = x-1;
                    auxY = y+1; 
                    encontrado = false;
                    while((!encontrado) && (auxX>=0 && auxX<this.tablero.length) && (auxY>=0 && auxY<this.tablero[0].length)){
                        if(this.tablero[auxX--][auxY++].equals("R")){
                            cantidad++;
                            encontrado = true;
                        }
                    }
                    auxX = x+1;
                    auxY = y-1;
                    encontrado = false;
                    while((!encontrado) && (auxX>=0 && auxX<this.tablero.length) && (auxY>=0 && auxY<this.tablero[0].length)){
                        if(this.tablero[auxX++][auxY--].equals("R")){
                            cantidad++;
                            encontrado = true;
                        }
                    }
                    
                    //Evaluamos en diagonal \
                    auxX = x-1;
                    auxY = y-1; 
                    encontrado = false;
                    while((!encontrado) && (auxX>=0 && auxX<this.tablero.length) && (auxY>=0 && auxY<this.tablero[0].length)){
                        if(this.tablero[auxX--][auxY--].equals("R")){
                            cantidad++;
                            encontrado = true;
                        }
                    }
                    auxX = x+1;
                    auxY = y+1;
                    encontrado = false;
                    while((!encontrado) && (auxX>=0 && auxX<this.tablero.length) && (auxY>=0 && auxY<this.tablero[0].length)){
                        if(this.tablero[auxX++][auxY++].equals("R")){
                            cantidad++;
                            encontrado = true;
                        }
                    }
                }
            }
        }
        return cantidad;
    }
    
    /**
     * Función que aplica la mutación en el 0.1% de los casos.
     * La mutación consiste en cambiar de posición las primeras 4 reinas encontradas al azar a posiciones vacías encontradas al azar.
     */
    public void verificarMutacion(){
        int x=0, y=0, posX=0, posY=0; 
        int random = (int) (Math.random() * (IA2_ProyNReinas.topeIteraciones - 1)) + 1; //Genero un aleatorio entre 1 y el tope de iteraciones
        int contador = 0;
        boolean encontrado = false;
        if (random <= (int) (IA2_ProyNReinas.topeIteraciones * 0.1 / 100)) { //Verifico que la mutación se aplique en el 0.1%
            while (contador <= 4) { //Cambio la posición de 4 reinas en el tablero de manera aleatoria.
                x = (int) Math.floor(Math.random() * 7);
                y = (int) Math.floor(Math.random() * 7);
                if (this.tablero[x][y].equals("R")) { //Si la posición encontrada tiene una reina
                    encontrado = false;
                    while (!encontrado) {
                        posX = (int) Math.floor(Math.random() * 7);
                        posY = (int) Math.floor(Math.random() * 7);
                        if (this.tablero[posX][posY].equals(".")) {  //Si la nueva posición está vacía
                            this.tablero[x][y] = "."; //Limpio la posición anterior
                            this.tablero[posX][posY] = "R"; //Cambio la reina a la nueva posición
                            contador++;
                            encontrado = true;
                        }
                    }

                }
            }
        }
    }
    
}
