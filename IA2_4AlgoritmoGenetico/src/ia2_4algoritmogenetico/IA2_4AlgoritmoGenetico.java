/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia2_4algoritmogenetico;

/**
 *
 * @author Herminio
 */
public class IA2_4AlgoritmoGenetico {
    public static int tamanioPoblacion = 100; //Debe ser un número par
    public static int numGeneraciones = 20;  //Número de iteraciones
    public static double probabilidadCruza = 0.8;
    public static double probabilidadMutacion = 0.1; 
    
    public static Variable varX = new Variable(22, 0.0, 4.0); //Variable X
    public static Variable varY = new Variable(22, 0.0, 4.0); //Variable Y
    public static int tamanioCadena = varX.getTamanio() + varY.getTamanio(); 
    
    public static int poblacionPadres[][] = new int[tamanioPoblacion][tamanioCadena];
    public static int poblacionSeleccionada[][] = new int[tamanioPoblacion][tamanioCadena];
    public static int poblacionHijos[][] = new int[tamanioPoblacion][tamanioCadena];
    public static int entero = 0, contador=0, individuo1 = 0, individuo2 = 0, individuoGanador = 0, puntoCruza = 0;
    public static double real = 0, funcion = 0, funcion2 = 0, realX = 0, realY = 0;
    public static boolean encontrado = false;
    public static String individuo;
    public static double aleatorio, mejorRealX = 0, mejorRealY = 0, mejorFuncion = 0;
    
    public static int binaryToDecimal(String numeroBinario) {
        int resultado = 0;//Aqui almacenaremos nuestra respuesta final
        int potencia = numeroBinario.length() - 1; //Numero de digitos que tiene nuestro binario menos 1
        for (int i = 0; i < numeroBinario.length(); i++) {//recorremos la cadena de numeros
            if (numeroBinario.charAt(i) == '1') {
                resultado += Math.pow(2, potencia);
            }
            potencia--; //Decremantamos la potencia
        }
        return resultado;
    }
    
    public static double decodificar(int x, Variable var){
        return var.getLim_inf() + x/(Math.pow(2, var.getTamanio())-1) * (var.getLim_sup() - var.getLim_inf()); 
    }
    
    public static double obtenerFuncion(double x, double y){
        // x^2
        //return Math.pow(x, 2);
        //double y = 300.0, m = 0.25, g = -32.17, k = 0.1;
        //return  y + (((m*g)/k)*x) - (((Math.pow(m, 2)*g)/(Math.pow(k, 2)))*(1.0-(Math.exp(((-k)*x)/m))));
        //return Math.pow(x,2) + Math.pow(y,2);
        //return Math.pow((x + (2*y)  -7), 2) + Math.pow(((2*x) + y  -5), 2);
        //return Math.sin(x + y) + Math.pow((x - y),2) - (1.5 * x) + (2.5 * y) + 1.0;
        //return (0.26*(Math.pow(x,2) + Math.pow(y,2))) - (0.48 * x * y);
        //return -Math.cos(x) * Math.cos(y) * Math.exp(-(Math.pow(x - Math.PI,2) + Math.pow(y - Math.PI,2)));
        double Tom = Math.sqrt(Math.pow(x,2) + Math.pow(1.2,2));
        double Dick = Math.sqrt(Math.pow(y,2) + Math.pow(2.5,2));
        double Mary = 4.3 - (x + y);
        return (Tom/2) + (Dick/4) + (Mary/6);
    }
    
    public static void generarPoblacion(){
        // Genero mi población
        for (int x = 0; x < poblacionPadres.length; x++) { //Lleno de 0 y 1 la población
            for (int y = 0; y < poblacionPadres[x].length; y++) {
                poblacionPadres[x][y] = (int) (Math.random() * (2 - 0)) + 0; //Asigno 0 ó 1
            }
        }

        for (int x = 0; x < poblacionPadres.length; x++) { 
            realX = obtenerReal(varX, 0, varX.getTamanio(), x); //Variable X
            realY = obtenerReal(varY, varX.getTamanio(), tamanioCadena, x); //Variable Y
            funcion = obtenerFuncion(realX, realY); //Aplico función 
            if (((mejorFuncion == 0) || (funcion < mejorFuncion))) {
                mejorRealX = realX;         //Guardo la mejor solución encontrada
                mejorRealY = realY;
                mejorFuncion = funcion;
            }
        }
    }
    
    public static void aplicarSeleccion(){
        contador = 0;
        while(contador < tamanioPoblacion){
            //Selecciono a los dos invididuos al azar
            individuo1 = (int) (Math.random() * (tamanioPoblacion)) + 1;  //Elijo al primer individuo
            encontrado = false;
            while(!encontrado){
                individuo2 = (int) (Math.random() * (tamanioPoblacion)) + 1; //Elijo al segundo individuo
                if((individuo1!=individuo2) ){ //Me aseguro que sean padres diferente, y que no sea un individuo seleccionado
                    encontrado=true;
                }
            }
            
            //Evaluamos al individuo 1
            realX = obtenerReal(varX, 0, varX.getTamanio(), individuo1-1);
            realY = obtenerReal(varY, varX.getTamanio(), tamanioCadena, individuo1-1);
            funcion = obtenerFuncion(realX, realY); //Aplico función 
            
            //Evaluamos al individuo 2
            realX = obtenerReal(varX, 0, varX.getTamanio(), individuo2-1);
            realY = obtenerReal(varY, varX.getTamanio(), tamanioCadena, individuo2-1);
            funcion2 = obtenerFuncion(realX, realY); //Aplico función 
            
            //Elijo al ganador
            if(funcion<funcion2){
                individuoGanador = individuo1;
            }else if(funcion2<funcion){
                individuoGanador = individuo2;
            }else{ //En el caso de que los valores obtenidos sean iguales.
                individuoGanador = individuo1;
            }
            for (int y=0; y < poblacionPadres[0].length; y++) { //Guardo el individuo en la nueva población
                poblacionSeleccionada[contador][y]= poblacionPadres[individuoGanador-1][y];
            }
            contador++;
        } //Fin de While
    }
    
    public static void aplicarCruza(){
        //Cruza
        contador = 0;
        int indice = 0;
        while (contador < (tamanioPoblacion / 2)) {
            aleatorio = ((Math.random() * (1 - 0)) + 0);
            if (aleatorio <= probabilidadCruza) { //Se aplica la cruza
                puntoCruza = (int) (Math.random() * (tamanioCadena - 2)) + 2;
                for (int y = 0; y < poblacionSeleccionada[0].length; y++) {
                    if (y < puntoCruza) {
                        poblacionHijos[indice][y] = poblacionSeleccionada[indice][y]; //Padre 1
                    } else {
                        poblacionHijos[indice][y] = poblacionSeleccionada[indice+1][y]; //Padre 2
                    }
                }
                for (int y = 0; y < poblacionSeleccionada[0].length; y++) {
                    if (y < puntoCruza) {
                        poblacionHijos[indice+1][y] = poblacionSeleccionada[indice][y]; //Padre 1
                    } else {
                        poblacionHijos[indice+1][y] = poblacionSeleccionada[indice+1][y]; //Padre 2
                    }
                }
            } else { //No aplico la cruza
                puntoCruza = 0;
                for (int y = 0; y < poblacionSeleccionada[0].length; y++) {
                    poblacionHijos[indice][y] = poblacionSeleccionada[indice][y]; //Padre 1
                    poblacionHijos[indice+1][y] = poblacionSeleccionada[indice+1][y]; //Padre 2
                }
            }
            indice = indice + 2;
            contador++;
        }   
    }
    
    public static void aplicarMutacion(){
        //Mutación
        int apuntador = 0;
        for (int x = 0; x < poblacionHijos.length; x++) {
            aleatorio = ((Math.random() * (1 - 0)) + 0);
            apuntador = 0;
            if (aleatorio <= probabilidadMutacion) { //Se aplica mutación
                apuntador = (int) ((Math.random() * (tamanioCadena - 0)) + 0);
                if (poblacionHijos[x][apuntador] == 0) { //Intercambio su valor en la posición obtenida al azar.
                    poblacionHijos[x][apuntador] = 1;
                } else {
                    poblacionHijos[x][apuntador] = 0;
                }
                apuntador++;
            }//Si no, no se aplica mutación            
        }//Fin de for
    }
    
    public static void aplicarElitismo(){
        for(int x=0; x < poblacionHijos.length; x++) {
            realX = obtenerReal(varX, 0, varX.getTamanio(), x); //Variable X
            realY = obtenerReal(varY, varX.getTamanio(), tamanioCadena, x); //Variable Y           
            funcion = obtenerFuncion(realX, realY); //Aplico función
            if(funcion < mejorFuncion){ 
                mejorRealX = realX;         //Guardo la mejor solución encontrada
                mejorRealY = realY;
                mejorFuncion = funcion;
            }
        }
    }
    
    public static void reemplazarPadres(){
        poblacionPadres = poblacionHijos; //La generación de los hijos pasa a ser la generación de padres, antes de comenzar la siguiente iteración.
    }
    
    public static double obtenerReal(Variable var, int inicio, int fin, int indice){
        individuo = "";
        for (int y = inicio; y < fin; y++) {
            individuo = individuo.concat(String.valueOf(poblacionPadres[indice][y]));
        }
        entero = binaryToDecimal(individuo); //Convierto los individuos en enteros
        return decodificar(entero, var); //Decodifico
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int contador = 1;
        generarPoblacion();
        System.out.printf("Generación "+contador+". \tx1 = %.6f, x2 = %.6f, f(x1,x2) = %.6f \n", mejorRealX, mejorRealY, mejorFuncion);
        while(contador < numGeneraciones){
            aplicarSeleccion();
            aplicarCruza();
            aplicarMutacion();
            aplicarElitismo();
            reemplazarPadres();
            contador++;
            System.out.printf("Generación "+contador+". \tx1 = %.6f, x2 = %.6f, f(x1,x2) = %.6f \n", mejorRealX, mejorRealY, mejorFuncion);
        }
    }//Fin de Main
    
}
