/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia2_aproypacman;


import java.util.TimerTask;
import static ia2_aproypacman.IA2_ProyPacman.pacman;
import static ia2_aproypacman.IA2_ProyPacman.f1;
import static ia2_aproypacman.IA2_ProyPacman.f2;
import static ia2_aproypacman.IA2_ProyPacman.f3;
import static ia2_aproypacman.IA2_ProyPacman.amb;
import static ia2_aproypacman.IA2_ProyPacman.timerPac;


/**
 *
 * @author Herminio
 */
class TiempoPac extends TimerTask{
    public void run() { //Esta función se ejecuta cada cierto tiempo        
        if(!pacman.moverPacman()){ //Genero el movimiento del pacman
            amb.imprimir(); //Imprimo el ambiente
            System.out.println("\n     [ F I N   D E L   J U E G O ]"); 
            timerPac.cancel(); //Cancelo el timer
        }else{
            amb.imprimir(); //Imprimo el ambiente
            f1.moverFantasma(); //Ejecuto siguiente movimiento del fantasma
            f2.moverFantasma(); //Ejecuto siguiente movimiento del fantasma
            f3.moverFantasma(); //Ejecuto siguiente movimiento del fantasma
            System.out.println("\n\n"); 
        } 
    }
}
