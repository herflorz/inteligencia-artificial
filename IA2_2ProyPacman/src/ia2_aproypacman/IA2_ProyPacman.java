/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia2_aproypacman;

import java.util.Timer;
/**
 *
 * @author Herminio
 */
public class IA2_ProyPacman {
    public static String[][] ambiente = new String[23][19]; //Defino el ambiente
    public final static String caracter_pacman = "O"; 
    public final static String caracter_fantasma = "A"; 
    public final static String caracter_obstaculo = "X";
    public static Ambiente amb = null;
    public static Fantasma f1 = null, f2 = null, f3 = null;
    public static Pacman pacman = null;
    public static Timer timerPac = null;
    public static int numFantasmas = 0;
       
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        amb = new Ambiente(); //Dibuja el ambiente
        f1 = new Fantasma();  //Creo el objeto del primer fantasma
        f2 = new Fantasma();  //Creo el objeto del segundo fantasma
        f3 = new Fantasma();  //Creo el objeto del tercer fantasma
        pacman = new Pacman();//Creo el objeto del pacman
        amb.imprimir(); //Imprime el ambiente
        
        timerPac = new Timer(); //Se inicia el timer
        timerPac.scheduleAtFixedRate(new TiempoPac(), 0, 400); //1000 es un segundo
    }
}
