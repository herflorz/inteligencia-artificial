/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia2_aproypacman;

import static ia2_aproypacman.IA2_ProyPacman.ambiente;
import static ia2_aproypacman.IA2_ProyPacman.caracter_obstaculo;
import static ia2_aproypacman.IA2_ProyPacman.caracter_fantasma;
import static ia2_aproypacman.IA2_ProyPacman.caracter_pacman;
import static ia2_aproypacman.IA2_ProyPacman.numFantasmas;

/**
 *
 * @author Herminio
 */
public class Ambiente{
    
    public Ambiente(){
        dibujarAmbiente(IA2_ProyPacman.caracter_obstaculo);  
    }
    
    //Asigna valores al ambiente, para simular los obstáculos y espacios libres
    public static void dibujarAmbiente(String caracter){
        //Lleno de vacíos el ambiente
        for (int x=0; x< ambiente.length; x++){
            for (int y = 0; y < ambiente[x].length; y++){
                ambiente[x][y] = " ";
            }
        }
        
        //Dibujo los obstáculos
        for(int y=0; y<ambiente[0].length; y++){
            ambiente[0][y] = caracter;  
            ambiente[22][y] = caracter;  
        }
        
        ambiente[0][0] = caracter; 
        ambiente[1][0] = caracter; 
        ambiente[2][0] = caracter; 
        ambiente[3][0] = caracter; 
        ambiente[4][0] = caracter; 
        ambiente[5][0] = caracter; 
        ambiente[6][0] = caracter;  ambiente[6][1] = caracter;  ambiente[6][2] = caracter;  ambiente[6][3] = caracter;  
        ambiente[7][0] = caracter;  ambiente[7][1] = caracter;  ambiente[7][2] = caracter;  ambiente[7][3] = caracter;  
        ambiente[8][0] = caracter;  ambiente[8][1] = caracter;  ambiente[8][2] = caracter;  ambiente[8][3] = caracter;  
        ambiente[9][0] = caracter;  ambiente[9][1] = caracter;  ambiente[9][2] = caracter;  ambiente[9][3] = caracter;    
        ambiente[11][0] = caracter; ambiente[11][1] = caracter;  ambiente[11][2] = caracter;  ambiente[11][3] = caracter; 
        ambiente[12][0] = caracter; ambiente[12][1] = caracter;  ambiente[12][2] = caracter;  ambiente[12][3] = caracter;
        ambiente[13][0] = caracter; ambiente[13][1] = caracter;  ambiente[13][2] = caracter;  ambiente[13][3] = caracter;
        ambiente[14][0] = caracter; ambiente[14][1] = caracter;  ambiente[14][2] = caracter;  ambiente[14][3] = caracter;   
        ambiente[15][0] = caracter; 
        ambiente[16][0] = caracter; 
        ambiente[17][0] = caracter; 
        ambiente[18][0] = caracter; ambiente[18][1] = caracter; 
        ambiente[19][0] = caracter; 
        ambiente[20][0] = caracter; 
        ambiente[21][0] = caracter; 
        ambiente[22][0] = caracter; 
        
        ambiente[0][18] = caracter; 
        ambiente[1][18] = caracter; 
        ambiente[2][18] = caracter; 
        ambiente[3][18] = caracter; 
        ambiente[4][18] = caracter; 
        ambiente[5][18] = caracter; 
        ambiente[6][18] = caracter;  ambiente[6][17] = caracter;  ambiente[6][16] = caracter;  ambiente[6][15] = caracter;  
        ambiente[7][18] = caracter;  ambiente[7][17] = caracter;  ambiente[7][16] = caracter;  ambiente[7][15] = caracter;  
        ambiente[8][18] = caracter;  ambiente[8][17] = caracter;  ambiente[8][16] = caracter;  ambiente[8][15] = caracter;  
        ambiente[9][18] = caracter;  ambiente[9][17] = caracter;  ambiente[9][16] = caracter;  ambiente[9][15] = caracter;    
        ambiente[11][18] = caracter; ambiente[11][17] = caracter;  ambiente[11][16] = caracter;  ambiente[11][15] = caracter; 
        ambiente[12][18] = caracter; ambiente[12][17] = caracter;  ambiente[12][16] = caracter;  ambiente[12][15] = caracter; 
        ambiente[13][18] = caracter; ambiente[13][17] = caracter;  ambiente[13][16] = caracter;  ambiente[13][15] = caracter; 
        ambiente[14][18] = caracter; ambiente[14][17] = caracter;  ambiente[14][16] = caracter;  ambiente[14][15] = caracter;   
        ambiente[15][18] = caracter; 
        ambiente[16][18] = caracter; 
        ambiente[17][18] = caracter; 
        ambiente[18][18] = caracter; ambiente[18][17] = caracter; 
        ambiente[19][18] = caracter; 
        ambiente[20][18] = caracter; 
        ambiente[21][18] = caracter; 
        ambiente[22][18] = caracter; 
        
        ambiente[0][9] = caracter;
        ambiente[1][9] = caracter;
        ambiente[2][9] = caracter;
        
        ambiente[4][5] = caracter;
        ambiente[5][5] = caracter;
        ambiente[6][5] = caracter;  ambiente[6][6] = caracter;  ambiente[6][7] = caracter;
        ambiente[7][5] = caracter;
        ambiente[8][5] = caracter;
        ambiente[9][5] = caracter;
        
        ambiente[4][13] = caracter;
        ambiente[5][13] = caracter;
        ambiente[6][13] = caracter;  ambiente[6][12] = caracter;  ambiente[6][11] = caracter;
        ambiente[7][13] = caracter;
        ambiente[8][13] = caracter;
        ambiente[9][13] = caracter;
        
        ambiente[11][5] = caracter; ambiente[11][13] = caracter;
        ambiente[12][5] = caracter; ambiente[12][13] = caracter;
        ambiente[13][5] = caracter; ambiente[13][13] = caracter;
        ambiente[14][5] = caracter; ambiente[14][13] = caracter;
        
        ambiente[14][7] = caracter;
        ambiente[14][8] = caracter;
        ambiente[14][9] = caracter;
        ambiente[14][10] = caracter;
        ambiente[14][11] = caracter;
        ambiente[15][9] = caracter;
        ambiente[16][9] = caracter;
        ambiente[17][9] = caracter;
        
        ambiente[20][11] = caracter;
        ambiente[20][12] = caracter;
        ambiente[20][13] = caracter;
        ambiente[20][14] = caracter;
        ambiente[20][15] = caracter;
        ambiente[20][16] = caracter;
        ambiente[18][13] = caracter;
        ambiente[19][13] = caracter;      
    }
    
    //Imprimo el ambiente
    public void imprimir(){
        System.out.print("\n\n"); //Limpio la pantalla, para imprimir la nueva posición del agente
        for (int x=0; x < ambiente.length; x++) {
            for (int y=0; y < ambiente[x].length; y++) {
                System.out.printf("%2s", ambiente[x][y]);
            }
        System.out.println("");  //Fin  
        }   
    }
    
    //Devuelve TRUE si la posición indicada esta OCUPADA
    public boolean estaOcupada_Pacman(int posX, int posY){
        boolean ocupada = false;
        if ((posX >= 0 && posX < ambiente.length) && (posY >= 0 && posY < ambiente[0].length)) {
            if((ambiente[posX][posY].equals(caracter_obstaculo)) || (ambiente[posX][posY].equals(caracter_pacman))){ //Si es una coordenada dentro del ambiente y está ocupada.
                ocupada = true; // Se marca como ocupada.
            }else if(ambiente[posX][posY].equals(caracter_fantasma)){
                numFantasmas++;
                ocupada = true; // Se marca como ocupada.
            }
        } else {
            ocupada = true;
        }
        return ocupada;
    }
    
    //Devuelve TRUE si la posición indicada esta OCUPADA
    public boolean estaOcupada_Fantasma(int posX, int posY){
        boolean ocupada = false;
        if ((posX >= 0 && posX < ambiente.length) && (posY >= 0 && posY < ambiente[0].length)) {
            if((ambiente[posX][posY].equals(caracter_obstaculo)) || (ambiente[posX][posY].equals(caracter_fantasma)) || (ambiente[posX][posY].equals(caracter_pacman))){ //Si es una coordenada dentro del ambiente y está ocupada.
                ocupada = true; // Se marca como ocupada.
            }
        } else {
            ocupada = true;
        }
        return ocupada;
    }
    
   
    
}
