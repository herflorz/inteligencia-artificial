/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia2_aproypacman;

import static ia2_aproypacman.IA2_ProyPacman.amb;
import static ia2_aproypacman.IA2_ProyPacman.ambiente;
import static ia2_aproypacman.IA2_ProyPacman.caracter_obstaculo;
import static ia2_aproypacman.IA2_ProyPacman.caracter_pacman;
import static ia2_aproypacman.IA2_ProyPacman.f1;
import static ia2_aproypacman.IA2_ProyPacman.f2;
import static ia2_aproypacman.IA2_ProyPacman.f3;
import static ia2_aproypacman.IA2_ProyPacman.pacman;
import static ia2_aproypacman.IA2_ProyPacman.numFantasmas;

/**
 *
 * @author Herminio
 */
public class Pacman {
    boolean s1 = false, s2 = false, s3 = false, s4 = false, s5 = false, s6 = false, s7 = false, s8 = false; //true Ocupada, false Libre
    public int direccionAgente = 0; 
    boolean encontrado = false, encontradoInterno = false, pasilloMagicoActivado = false;
    int posX = 0, posY = 0, posX_anterior = 0, posY_anterior = 0;
    int anterior_caso1 = 0, anterior_caso2 = 0, anterior_caso3 = 0, anterior_caso4 = 0, anterior_caso5 = 0, anterior_caso6 = 0, 
        anterior_caso7 = 0, anterior_caso8 = 0, anterior_caso9 = 0, anterior_caso10 = 0, anterior_caso11 = 0;
    int posX_aux, posY_aux;
    
    //Constructor
    public Pacman(){
        //Dibuja el agente en una posición aleatoria que no esté ocupada por un obstáculo u otro objeto
        this.direccionAgente = 0;
        encontrado = false;
        while(!encontrado){
            this.posX = (int) Math.floor(Math.random()*23);
            this.posY = (int) Math.floor(Math.random()*19);
            if(ambiente[this.posX][this.posY].equals(" ")){ //Si es una posición libre
               if((f1 == null && f2 == null && f3 == null && pacman == null) ||
                 ((f1 != null) && (f1.getPosX() != this.posX) && (f1.getPosY() != this.posY)) || //Verifico que no sea una posición de otro objeto.
                 ((f2 != null) && (f2.getPosX() != this.posX) && (f2.getPosY() != this.posY)) ||
                 ((f3 != null) && (f3.getPosX() != this.posX) && (f3.getPosY() != this.posY)) ||
                 ((pacman != null) && (pacman.getPosX() != this.posX) && (pacman.getPosY() != this.posY))){
                    ambiente[this.posX][this.posY] = caracter_pacman;
                    encontrado = true;
                  
               }
            }  
        }
//        this.posX = 10;
//        this.posY = 18;
//        ambiente[this.posX][this.posY] = caracter_pacman;
    }

    public int getPosX() {
        return this.posX;
    }

    public int getPosY() {
        return this.posY;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }
    
    public void actualizarSensores(){
        //Sensores del pacman
        numFantasmas = 0; //Reinicio los contadores de los obstaculos
        //SOLO AL PACMAN LE HABILITO SUS 8 SENSORES
        s1 = amb.estaOcupada_Pacman(getPosX()-1, getPosY()-1); 
            s2 = amb.estaOcupada_Pacman(getPosX()-1, getPosY()); 
        s3 = amb.estaOcupada_Pacman(getPosX()-1, getPosY()+1); 
        if(getPosX() == 10 && getPosY() == 18){
            s4 = false;
        }else{
            s4 = amb.estaOcupada_Pacman(getPosX(), getPosY()+1);
        }
        s5 = amb.estaOcupada_Pacman(getPosX()+1, getPosY()+1);
            s6 = amb.estaOcupada_Pacman(getPosX()+1, getPosY());
        s7 = amb.estaOcupada_Pacman(getPosX()+1, getPosY()-1);
        if(getPosX() == 10 && getPosY() == 0){
            s8 = false;
        }else{
           s8 = amb.estaOcupada_Pacman(getPosX(), getPosY()-1); 
        }
            
    }
    
    
    public void obtenerNuevaPosicion(){
        switch (this.direccionAgente) {
            case 1: //Norte
                this.posX_aux--;
                break;
            case 2: //Sur
                this.posX_aux++; 
                break;
            case 3: //Este
                this.posY_aux++; 
                break;
            case 4: //Oeste
                this.posY_aux--; 
                break;
        }

    }
    
    public int obtenerDireccionConRespectoAFantasmas(){
        int direccionContraria = 0;
        //Distancias entre fantasma 1 y pacman
        int f1_posX = f1.getPosX();
        int f1_posY = f1.getPosY();
        double df1_N = Math.sqrt((Math.pow((f1_posX - (posX_aux - 1)), 2) + Math.pow((f1_posY - (posY_aux)), 2)));
        double df1_S = Math.sqrt((Math.pow((f1_posX - (posX_aux + 1)), 2) + Math.pow((f1_posY - (posY_aux)), 2)));
        double df1_E = Math.sqrt((Math.pow((f1_posX - (posX_aux)), 2) + Math.pow((f1_posY - (posY_aux + 1)), 2)));
        double df1_O = Math.sqrt((Math.pow((f1_posX - (posX_aux)), 2) + Math.pow((f1_posY - (posY_aux - 1)), 2)));

        //Distancias entre fantasma 2 y pacman
        int f2_posX = f2.getPosX();
        int f2_posY = f2.getPosY();
        double df2_N = Math.sqrt((Math.pow((f2_posX - (posX_aux - 1)), 2) + Math.pow((f2_posY - (posY_aux)), 2)));
        double df2_S = Math.sqrt((Math.pow((f2_posX - (posX_aux + 1)), 2) + Math.pow((f2_posY - (posY_aux)), 2)));
        double df2_E = Math.sqrt((Math.pow((f2_posX - (posX_aux)), 2) + Math.pow((f2_posY - (posY_aux + 1)), 2)));
        double df2_O = Math.sqrt((Math.pow((f2_posX - (posX_aux)), 2) + Math.pow((f2_posY - (posY_aux - 1)), 2)));

        //Distancias entre fantasma 3 y pacman
        int f3_posX = f3.getPosX();
        int f3_posY = f3.getPosY();
        double df3_N = Math.sqrt((Math.pow((f3_posX - (posX_aux - 1)), 2) + Math.pow((f3_posY - (posY_aux)), 2)));
        double df3_S = Math.sqrt((Math.pow((f3_posX - (posX_aux + 1)), 2) + Math.pow((f3_posY - (posY_aux)), 2)));
        double df3_E = Math.sqrt((Math.pow((f3_posX - (posX_aux)), 2) + Math.pow((f3_posY - (posY_aux + 1)), 2)));
        double df3_O = Math.sqrt((Math.pow((f3_posX - (posX_aux)), 2) + Math.pow((f3_posY - (posY_aux - 1)), 2)));

        //Obtengo la distancia más corta obtenida y me muevo hacia el sentido contrario.
        if (((df1_N < df1_S) && (df1_N < df1_E) && (df1_N < df1_O) && (df1_N < df2_N) && (df1_N < df2_S) && (df1_N < df2_E) && (df1_N < df2_O) && (df1_N < df3_N) && (df1_N < df3_S) && (df1_N < df3_E) && (df1_N < df3_O))
                || ((df2_N < df1_N) && (df2_N < df1_S) && (df2_N < df1_E) && (df2_N < df1_O) && (df2_N < df2_S) && (df2_N < df2_E) && (df2_N < df2_O) && (df2_N < df3_N) && (df2_N < df3_S) && (df2_N < df3_E) && (df2_N < df3_O))
                || ((df3_N < df1_N) && (df3_N < df1_S) && (df3_N < df1_E) && (df3_N < df1_O) && (df3_N < df2_N) && (df3_N < df2_S) && (df3_N < df2_E) && (df3_N < df2_O) && (df3_N < df3_S) && (df3_N < df3_E) && (df3_N < df3_O))) {
            direccionContraria = 2; //Entonces se mueve al sentido contrario
        } else if (((df1_S < df1_N) && (df1_S < df1_E) && (df1_S < df1_O) && (df1_S < df2_N) && (df1_S < df2_S) && (df1_S < df2_E) && (df1_S < df2_O) && (df1_S < df3_N) && (df1_S < df3_S) && (df1_S < df3_E) && (df1_S < df3_O))
                || ((df2_S < df1_N) && (df2_S < df1_S) && (df2_S < df1_E) && (df2_S < df1_O) && (df2_S < df2_N) && (df2_S < df2_E) && (df2_S < df2_O) && (df2_S < df3_N) && (df2_S < df3_S) && (df2_S < df3_E) && (df2_S < df3_O))
                || ((df3_S < df1_N) && (df3_S < df1_S) && (df3_S < df1_E) && (df3_S < df1_O) && (df3_S < df2_N) && (df3_S < df2_S) && (df3_S < df2_E) && (df3_S < df2_O) && (df3_S < df3_N) && (df3_S < df3_E) && (df3_S < df3_O))) {
            direccionContraria = 1;
        } else if (((df1_E < df1_N) && (df1_E < df1_S) && (df1_E < df1_O) && (df1_E < df2_N) && (df1_E < df2_S) && (df1_E < df2_E) && (df1_E < df2_O) && (df1_E < df3_N) && (df1_E < df3_S) && (df1_E < df3_E) && (df1_E < df3_O))
                || ((df2_E < df1_N) && (df2_E < df1_S) && (df2_E < df1_E) && (df2_E < df1_O) && (df2_E < df2_N) && (df2_E < df2_S) && (df2_E < df2_O) && (df2_E < df3_N) && (df2_E < df3_S) && (df2_E < df3_E) && (df2_E < df3_O))
                || ((df3_E < df1_N) && (df3_E < df1_S) && (df3_E < df1_E) && (df3_E < df1_O) && (df3_E < df2_N) && (df3_E < df2_S) && (df3_E < df2_E) && (df3_E < df2_O) && (df3_E < df3_N) && (df3_E < df3_S) && (df3_E < df3_O))) {
            direccionContraria = 4;
        } else if (((df1_O < df1_N) && (df1_O < df1_S) && (df1_O < df1_E) && (df1_O < df2_N) && (df1_O < df2_S) && (df1_O < df2_E) && (df1_O < df2_O) && (df1_O < df3_N) && (df1_O < df3_S) && (df1_O < df3_E) && (df1_O < df3_O))
                || ((df2_O < df1_N) && (df2_O < df1_S) && (df2_O < df1_E) && (df2_O < df1_O) && (df2_O < df2_N) && (df2_O < df2_S) && (df2_O < df2_E) && (df2_O < df3_N) && (df2_O < df3_S) && (df2_O < df3_E) && (df2_O < df3_O))
                || ((df3_O < df1_N) && (df3_O < df1_S) && (df3_O < df1_E) && (df3_O < df1_O) && (df3_O < df2_N) && (df3_O < df2_S) && (df3_O < df2_E) && (df3_O < df2_O) && (df3_O < df3_N) && (df3_O < df3_S) && (df3_O < df3_E))) {
            direccionContraria = 3;
        }
        return direccionContraria;
    }
    
    public boolean moverPacman(){
        actualizarSensores();
        if((numFantasmas==3) || (s2 && s4 && s6 && s8)){ 
            //El juego termina cuando los 3 fantasmas acorralan al pacman o cuando no pueda moverse porque su N, S, E, O estan opcupadas.
            return false; //Disparo el fin del juego
        }else{
            posX_anterior = getPosX(); //Respaldo la posición X anterior antes de cualquier cambio.
            posY_anterior = getPosY(); //Respaldo la posición Y anterior antes de cualquier cambio.
            posX_aux = posX_anterior; //Servirá para obtener la nueva posición X a partir de la actual
            posY_aux = posY_anterior; //Servirá para obtener la nueva posición Y a partir de la actual

            if((this.posX != 10) && (this.posY != 18)){ //Si no es el pasillo mágico
                direccionAgente = obtenerDireccionConRespectoAFantasmas();
            }else{
                direccionAgente = 0;
            }
                    
            encontrado = false;
            pasilloMagicoActivado = false;
            while (!encontrado) {
                encontradoInterno = false;
                if (direccionAgente == 1 && !s2) { //Si el norte esta libre
                    encontrado = true;   
                } else if (direccionAgente == 2 && !s6) { //Si el sur esta libre
                    encontrado = true;
                } else if (direccionAgente == 3 && !s4) { //Si el este esta libre
                    encontrado = true;
                } else if (direccionAgente == 4 && !s8) { //Si el oeste esta libre 
                    encontrado = true;
                } else {
                    if ((!s2) && (!s4) && (!s6) && (!s8)) { //Si los 4 sensores posibles no estan ocupados //Caso 1
                        while (!encontradoInterno) {
                            direccionAgente = (int) (Math.random() * (4 - 1)) + 1; //Se genera un nuevo aleatorio entre 1 y 4
                            if (direccionAgente != anterior_caso1) {
                                encontradoInterno = true;
                            }
                        }
                        anterior_caso1 = direccionAgente;
                    } else if ((s2) && (!s4) && (!s6) && (!s8)) { //Caso 2
                        while (!encontradoInterno) {
                            direccionAgente = (int) (Math.random() * (4 - 2)) + 2; //Se genera un nuevo aleatorio entre 1 y 4
                            if (direccionAgente != anterior_caso2) {
                                encontradoInterno = true;
                            }
                        }
                        anterior_caso2 = direccionAgente;
                    } else if ((!s2) && (s4) && (!s6) && (!s8)) { //Caso 3
                        while (!encontradoInterno) {
                            direccionAgente = (int) (Math.random() * (4 - 1)) + 1; //Se genera un nuevo aleatorio entre 1 y 4
                            if (direccionAgente != 3) {
                                encontradoInterno = true;
                            }
                        }
                        anterior_caso3 = direccionAgente;
                    } else if ((!s2) && (!s4) && (s6) && (!s8)) { //Caso 4
                        while (!encontradoInterno) {
                            direccionAgente = (int) (Math.random() * (4 - 1)) + 1; //Se genera un nuevo aleatorio entre 1 y 4
                            if (direccionAgente != 2) {
                                encontradoInterno = true;
                            }
                        }
                        anterior_caso4 = direccionAgente;
                    } else if ((!s2) && (!s4) && (!s6) && (s8)) { //Caso 5
                        while (!encontradoInterno) {
                            direccionAgente = (int) (Math.random() * (4 - 1)) + 1; //Se genera un nuevo aleatorio entre 1 y 4
                            if (direccionAgente != 4) {
                                encontradoInterno = true;
                            }
                        }
                        anterior_caso5 = direccionAgente;
                    } else if ((s2) && (!s4) && (!s6) && (s8)) { //Caso 6
                        direccionAgente = (int) (Math.random() * (3 - 2)) + 2; //Se genera un nuevo aleatorio entre 2 y 3
                        anterior_caso6 = direccionAgente;
                    } else if ((s2) && (s4) && (!s6) && (!s8)) { //Caso 7
                        while (!encontradoInterno) {
                            direccionAgente = (int) (Math.random() * (4 - 2)) + 2; //Se genera un nuevo aleatorio entre 1 y 4
                            if (direccionAgente != 1 && direccionAgente != 3) {
                                encontradoInterno = true;
                            }
                        }
                        anterior_caso7 = direccionAgente;
                    } else if ((!s2) && (!s4) && (s6) && (s8)) { //Caso 8
                        while (!encontradoInterno) {
                            direccionAgente = (int) (Math.random() * (4 - 1)) + 1; //Se genera un nuevo aleatorio entre 1 y 4
                            if (direccionAgente != 2 && direccionAgente != 4) {
                                encontradoInterno = true;
                            }
                        }
                        anterior_caso8 = pacman.direccionAgente;
                    } else if ((!s2) && (s4) && (s6) && (!s8)) { //Caso 9
                        while (!encontradoInterno) {
                            direccionAgente = (int) (Math.random() * (4 - 1)) + 1; //Se genera un nuevo aleatorio entre 1 y 4
                            if (direccionAgente != 2 && direccionAgente != 3) {
                                if (direccionAgente == anterior_caso9) {
                                    direccionAgente = 4;
                                }
                                encontradoInterno = true;
                            }
                        }
                        anterior_caso9 = direccionAgente;
                    } else if ((s2) && (!s4) && (s6) && (!s8)) { //Caso 10
                        if ((!pasilloMagicoActivado) && (posX_aux == 10 && posY_aux == 18)) {
                            posY_aux = 0;
                            pasilloMagicoActivado = true;
                        } else {
                            while (!encontradoInterno) {
                                direccionAgente = (int) (Math.random() * (4 - 3)) + 3; //Se genera un nuevo aleatorio entre 1 y 4
                                if (direccionAgente != 1 && direccionAgente != 2) {
                                    encontradoInterno = true;
                                }
                            }
                            anterior_caso10 = direccionAgente;
                        }
                    } else if ((!s2) && (s4) && (!s6) && (s8)) { //Caso 11
                        while (!encontradoInterno) {
                            direccionAgente = (int) (Math.random() * (4 - 1)) + 1; //Se genera un nuevo aleatorio entre 1 y 4
                            if (direccionAgente != 3 && direccionAgente != 4) {
                                encontradoInterno = true;
                            }
                        }
                        anterior_caso11 = direccionAgente;
                    } else if ((s2) && (s4) && (s6) && (!s8)) { //Caso 12 NO APLICA POR EL AMBIENTE ACTUAL
                        if ((!pasilloMagicoActivado) && (posX_aux == 10 && posY_aux == 18)) {
                            posY_aux = 0;
                            pasilloMagicoActivado = true;
                        } else {
                            direccionAgente = 4;
                        }
                    } else if ((!s2) && (s4) && (s6) && (s8)) { //Caso 13 NO APLICA POR EL AMBIENTE ACTUAL
                        direccionAgente = 1;

                    } else if ((s2) && (!s4) && (s6) && (s8)) { //Caso 14 NO APLICA POR EL AMBIENTE ACTUAL
//                        if ((!pasilloMagicoActivado) && (posX_aux == 10 && posY_aux == 0)) {
//                            posY_aux = 18;
//                            pasilloMagicoActivado = true;
//                        } else {

                                direccionAgente = 3;
                          
//                        }
                    } else if ((s2) && (s4) && (!s6) && (s8)) { //Caso 15 NO APLICA POR EL AMBIENTE ACTUAL
                        direccionAgente = 2;
                    }
                }

                if (!pasilloMagicoActivado) {
                    obtenerNuevaPosicion();
                }else{
                    pasilloMagicoActivado = false;
                }
                
                //Cuido que los nuevos valores de X y Y estén dentro del ambiente.
                if (posX_aux < 0) {
                    posX_aux = 0;
                }
                if (posX_aux > ambiente.length - 1) {
                    posX_aux = ambiente.length - 1;
                }
                if (posY_aux < 0) {
                    posY_aux = 0;
                }
                if (posY_aux > ambiente[0].length - 1) {
                    posY_aux = ambiente[0].length - 1;
                }
            
                //Que los agentes no se traslapen
                if ((!encontrado) || (((f1 != null) && (f1.getPosX() != posX_aux) && (f1.getPosY() != posY_aux))
                        || ((f2 != null) && (f2.getPosX() != posX_aux) && (f2.getPosY() != posY_aux))
                        || ((f3 != null) && (f3.getPosX() != posX_aux) && (f3.getPosY() != posY_aux))
                        || ((pacman != null) && (pacman.getPosX() != posX_aux) && (pacman.getPosY() != posY_aux)))) {
                    if (ambiente[posX_aux][posY_aux] != caracter_obstaculo) {
                        encontrado = true;
                    }
                }
            }     

            ambiente[posX_anterior][posY_anterior] = " "; //Limpio la posición actual del agente
            this.setPosX(posX_aux); //Actualizo la nueva posición X del agente
            this.setPosY(posY_aux); //Actualizo la nueva posición Y del agente
            ambiente[posX_aux][posY_aux] = caracter_pacman; //Dibujo el agente en su nueva posición 
            return true; //Contunúa el juego
        }
        
    }
    
    
}
